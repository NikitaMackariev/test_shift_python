from typing import Optional

from pydantic import BaseModel


class SalaryGetSchema(BaseModel):
    date_received: str


class SalaryInfoSchema(BaseModel):
    official_salary: float
    date_received: str
    income_tax: float
    bonus: float
    total_salary: Optional[float]


class SalaryToDB(BaseModel):
    id: int
    official_salary: float
    income_tax: float
    bonus: float
    date_received: str
    user_id: int
