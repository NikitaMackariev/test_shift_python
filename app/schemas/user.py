from pydantic import EmailStr, BaseModel, validator
from typing import Optional, Dict, Any
from datetime import datetime, date


class UserBase(BaseModel):
    username: str
    name: str
    second_name: str
    last_name: str
    email: EmailStr

    class Config:
        orm_mode = True


class UserCreate(UserBase):
    password: str

    class Config:
        orm_mode = True

    def create_update_dict(self) -> Dict[str, Any]:
        return self.dict(exclude_unset=True)


class UserUpdate(UserBase):
    password: str

    class Config:
        orm_mode = True

    def create_update_dict(self) -> Dict[str, Any]:
        return self.dict(exclude_unset=True)


class UserRead(UserBase):
    id: int
    planing_date_of_promotion: str = None

    class Config:
        orm_mode = True

    @validator('planing_date_of_promotion', pre=True)
    def format_date_of_promotion(cls, value):
        if value:
            return value.strftime('%d.%m.%Y')
        return None


class DateOfPromotionSchema(BaseModel):
    position_name: Optional[str]
    planing_date_of_promotion: str = None

    class Config:
        orm_mode = True

    @validator('planing_date_of_promotion', pre=True)
    def format_date_of_promotion(cls, value):
        if value is not None and isinstance(value, str):
            value = datetime.strptime(value, '%d.%m.%Y')
        return value.strftime('%d.%m.%Y') if value else None


class UserToDB(BaseModel):
    username: str
    name: str
    second_name: str
    last_name: str
    email: EmailStr
    hashed_password: str
    position_name: str
    created_at: datetime
    updated_at: datetime
    is_active: bool
    is_superuser: bool
    planing_date_of_promotion: date = None

    class Config:
        orm_mode = True

    @validator('planing_date_of_promotion', pre=True)
    def format_date_of_promotion(cls, value):
        if value is not None:
            try:
                datetime.strptime(value, '%Y-%m-%d')
            except ValueError:
                raise ValueError('Incorrect date format, should be YYYY-MM-DD')
            return datetime.strptime(value, '%Y-%m-%d').date()
