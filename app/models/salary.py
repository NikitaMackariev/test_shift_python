from sqlalchemy import Column, Float, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from app.core.db import Base


class Salary(Base):
    """Модель заработной платы."""

    __tablename__ = 'salaries'

    official_salary = Column(Float)
    income_tax = Column(Float, default=0.87)
    bonus = Column(Float)
    date_received = Column(String, nullable=True)
    user_id = Column(Integer, ForeignKey('users.id'), nullable=True)
    user = relationship('User', back_populates='salary')

    @property
    def total_salary(self):
        return self.calculate_total_salary(self.official_salary, self.bonus, self.income_tax)

    @classmethod
    def calculate_total_salary(cls, official_salary, bonus, income_tax):
        if not isinstance(official_salary, (int, float)) or not isinstance(bonus, (int, float)):
            return 0
        return round((official_salary + bonus) * income_tax, 2)
