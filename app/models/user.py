from datetime import datetime, timedelta

from sqlalchemy import Boolean, Column, Date, DateTime, String
from sqlalchemy.orm import relationship

from app.core.db import Base


def default_promotion_date(context):
    created_at = context.get_current_parameters()['created_at']
    return (created_at + timedelta(days=365))


class User(Base):
    """Модель пользователя."""

    __tablename__ = 'users'

    username = Column(String(50), nullable=False)
    name = Column(String(20), nullable=False)
    second_name = Column(String(20))
    last_name = Column(String(20), nullable=False)
    email = Column(String(100), unique=True, nullable=False)
    hashed_password = Column(String(150))
    position_name = Column(String, nullable=True)
    created_at = Column(DateTime, default=datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)
    is_active = Column(Boolean, default=True)
    is_superuser = Column(Boolean, default=False)
    planing_date_of_promotion = Column(
        Date,
        default=default_promotion_date,
        nullable=True
    )
    salary = relationship('Salary', back_populates='user')

    @property
    def next_promotion_date(self):
        if self.date_of_promotion:
            return self.planing_date_of_promotion.strftime('%d.%m.%Y')
        else:
            last_change_date = self.updated_at if self.updated_at else self.created_at
            next_promotion = last_change_date + timedelta(days=365)
            return next_promotion.strftime('%d.%m.%Y')

    def update_promotion_date(self):
        today = datetime.utcnow().date()

        if today >= self.date_of_promotion.date():
            self.date_of_promotion = datetime.utcnow() + timedelta(days=365)
