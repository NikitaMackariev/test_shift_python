from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession

from app.api.services.salary import salary_service
from app.core.db import get_async_session
from app.core.user import current_user
from app.schemas.salary import SalaryGetSchema

router = APIRouter()


@router.post('/salary',
             tags=['salary'])
async def get_user_salary(
    date_received: SalaryGetSchema,
    session: AsyncSession = Depends(get_async_session),
    current_user: str = Depends(current_user),
):
    return await salary_service.get_user_salary(
        date_received, session, current_user
    )
