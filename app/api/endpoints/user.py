from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession

from app.core.db import get_async_session
from app.core.user import auth_backend, current_user, fastapi_users
from app.models.user import User
from app.schemas.user import (DateOfPromotionSchema, UserCreate, UserRead,
                              UserUpdate)

router = APIRouter()

router.include_router(
    fastapi_users.get_auth_router(auth_backend),
    prefix='/auth/jwt',
    tags=['auth'],
)
router.include_router(
    fastapi_users.get_register_router(user_create_schema=UserCreate,
                                      user_schema=UserRead),
    prefix='/auth',
    tags=['auth'],
)
router.include_router(
    fastapi_users.get_users_router(UserRead, UserUpdate),
    prefix='/users',
    tags=['users'],
)


@router.get(
    '/promotion',
    response_model=DateOfPromotionSchema,
    tags=['date of promotion'])
async def get_user_date_promotion(
    session: AsyncSession = Depends(get_async_session),
    current_user: User = Depends(current_user)
):
    return DateOfPromotionSchema(
        position_name=current_user.position_name,
        planing_date_of_promotion=current_user.planing_date_of_promotion
    )
