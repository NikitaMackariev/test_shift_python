from fastapi import Depends
from sqlalchemy import and_, select
from sqlalchemy.ext.asyncio import AsyncSession

from app.api.exceptions import NoSallaryError
from app.api.services.base import BaseService
from app.core.db import get_async_session
from app.core.user import current_user
from app.crud.salary import salary_crud
from app.models.salary import Salary
from app.schemas.salary import SalaryGetSchema, SalaryInfoSchema


class SalaryService(BaseService):
    """Сервис для работы с заработной платой."""

    async def get_user_salary(
        self,
        date_received: SalaryGetSchema,
        session: AsyncSession = Depends(get_async_session),
        current_user: str = Depends(current_user),
    ):
        try:
            salary = (await session.execute(
                select(Salary).where(and_(Salary.date_received == date_received.date_received, Salary.user_id == current_user.id)))).scalars().first()
            return SalaryInfoSchema(
                official_salary=salary.official_salary,
                date_received=salary.date_received,
                income_tax=salary.income_tax,
                bonus=salary.bonus,
                total_salary=salary.total_salary
            )
        except Exception:
            raise NoSallaryError()


salary_service = SalaryService(salary_crud)
