import logging
from typing import Optional, Union

import jwt
from fastapi import Depends, Header, HTTPException, Request
from fastapi_users import (BaseUserManager, FastAPIUsers, IntegerIDMixin,
                           InvalidPasswordException)
from fastapi_users.authentication import (AuthenticationBackend,
                                          BearerTransport, JWTStrategy)
from fastapi_users_db_sqlalchemy import SQLAlchemyUserDatabase
from sqlalchemy.ext.asyncio import AsyncSession

from app.core.config import settings
from app.core.db import get_async_session
from app.models.user import User
from app.schemas.user import UserCreate


def get_current_user_id(authorization: Optional[str] = Header(None)):
    """Получение user_id по токену."""

    if not authorization:
        raise HTTPException(status_code=403, detail='Unauthorized')
    try:
        payload = jwt.decode(authorization, settings.secret_key, algorithms=['HS256'])
        return payload['user_id']
    except jwt.PyJWTError:
        raise HTTPException(status_code=403, detail='Invalid token')


async def get_user_db(session: AsyncSession = Depends(get_async_session)):

    """Асинхронный генератор. Обеспечивает доступ к базе данных через SQLAlchemy."""
    yield SQLAlchemyUserDatabase(session, User)

bearer_transport = BearerTransport(tokenUrl='auth/jwt/login')


def get_jwt_strategy() -> JWTStrategy:

    """Хранение токена в виде JWT."""
    return JWTStrategy(secret=settings.secret_key, lifetime_seconds=3600)


auth_backend = AuthenticationBackend(
    name='jwt',
    transport=bearer_transport,
    get_strategy=get_jwt_strategy
)


class UserManager(IntegerIDMixin, BaseUserManager[User, int]):
    """Валидация пароля и действие при успешной регистрации."""

    async def validate_password(
        self,
        password: str,
        user: Union[UserCreate, User],
    ) -> None:
        if len(password) < 3:
            raise InvalidPasswordException(
                reason='Длина пароля не может быть менее 3 символов'
            )
        if user.email in password:
            raise InvalidPasswordException(
                reason='Пароль не должен содержать адрес электронной почты'
            )

    async def on_after_register(
            self, user: User, request: Optional[Request] = None
    ):
        """Сообщает в консоль о регистрации."""
        logging.info(f'Пользователь {user.email} зарегистрирован.')


async def get_user_manager(user_db=Depends(get_user_db)):
    yield UserManager(user_db)

fastapi_users = FastAPIUsers[User, int](
    get_user_manager,
    [auth_backend],
)

current_user = fastapi_users.current_user(active=True)
