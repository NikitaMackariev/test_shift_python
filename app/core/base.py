"""Импорты класса Base и всех моделей для Alembic."""

from .db import Base  # noqa
from app.models import User, Salary # noqa
