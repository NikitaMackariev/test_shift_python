from pydantic import BaseSettings


class Settings(BaseSettings):
    app_title: str = 'Приложение Working days.'
    description: str = 'Сервис для просмотра зарплаты и даты следующего повышения.'
    database_url: str = 'sqlite+aiosqlite:///./fastapi.db'
    secret_key: str = 'SECRET'

    class Config:
        env_file = '.env'


settings = Settings()
