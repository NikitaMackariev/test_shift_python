from app.crud.base import CRUDBase
from app.models.salary import Salary


class CRUDSalary(CRUDBase):
    pass


salary_crud = CRUDSalary(Salary)