import asyncio
import contextlib
import csv

from sqlalchemy.exc import IntegrityError

from app.core.db import get_async_session
from app.crud.salary import salary_crud
from app.crud.user import user_crud
from app.schemas.salary import SalaryToDB
from app.schemas.user import UserToDB

get_async_session_context = contextlib.asynccontextmanager(get_async_session)


async def add_users():
    """Добавление пользователей в базу данных для тестирования."""

    with open('./data/users.csv') as csvfile:
        reader = csv.reader(csvfile)
        next(reader)
        async with get_async_session_context() as session:
            for row in reader:
                users_in = UserToDB(
                    id=row[0],
                    username=row[1],
                    name=row[2],
                    second_name=row[3],
                    last_name=row[4],
                    email=row[5],
                    hashed_password=row[6],
                    position_name=row[7],
                    created_at=row[8],
                    updated_at=row[9],
                    is_active=row[10],
                    is_superuser=row[11],
                    planing_date_of_promotion=row[12]
                )
                await user_crud.create(users_in, session=session)
            print('Users added to DB.')


async def add_salaries():
    """Добавление заработной платы для тестирования."""

    with open('./data/salary.csv') as csvfile:
        reader = csv.reader(csvfile)
        next(reader)
        async with get_async_session_context() as session:
            for row in reader:
                salary_in = SalaryToDB(
                    id=row[0],
                    official_salary=row[1],
                    income_tax=row[2],
                    bonus=row[3],
                    date_received=row[4],
                    user_id=row[5]
                )
                await salary_crud.create(salary_in, session=session)
            print('Salaries added to DB.')


async def fill_db():
    """Формирование базы данных для тестирования."""
    try:
        await add_users()
        await add_salaries()
    except IntegrityError:
        print('The database is already full.')


asyncio.run(fill_db())