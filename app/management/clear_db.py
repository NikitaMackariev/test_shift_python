import asyncio
import contextlib

from sqlalchemy import delete

from app.core.db import get_async_session
from app.models import Salary, User

get_async_session_context = contextlib.asynccontextmanager(get_async_session)


async def delete_all_in_table(model):
    """Удаление данных в таблице базы данных."""

    async with get_async_session_context() as session:
        await session.execute(
            delete(model)
        )
        await session.commit()
    print(f'All {model.__name__} deleted from DB.')


async def clear_db():
    """Очистка базы данных."""

    await delete_all_in_table(Salary)
    await delete_all_in_table(User)


asyncio.run(clear_db())
