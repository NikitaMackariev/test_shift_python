# Сервис просмотра заработной платы и даты следующего повышения
***
## Технологии проекта:
[![Python](https://img.shields.io/badge/-Python%203.9-464646?style=flat&logo=Python)](https://www.python.org/)
[![FastAPI](https://img.shields.io/badge/-FastAPI-464646?style=flat&logo=FastAPI)](https://fastapi.tiangolo.com/)
[![Pydantic](https://img.shields.io/badge/-Pydantic-464646?style=flat&logo=Pydantic)](https://docs.pydantic.dev/)
[![SQLAlchemy](https://img.shields.io/badge/-SQLAlchemy-464646?style=flat&logo=SQLAlchemy)](https://www.sqlalchemy.org/)
[![Alembic](https://img.shields.io/badge/-Alembic-464646?style=flat&logo=Alembic)](https://alembic.sqlalchemy.org/en/latest/)
***

## REST-сервис просмотра текущей зарплаты и даты следующего повышения.
Сотрудник, получив JWT-Token после аутентификации по электронной почте и паролю, может посмотреть свою заработную плату за определенный месяц, а также дату следующего повышения.  **Срок действия токена - 1 час**. Дата следующего повышения по умолчанию составляет спустя год после регистрации (приема на работу) пользователя. Также работодатель (администратор) вправе сам установить дату повышения, и в случае оставления в прежней должности дата повышения автоматически продлевается на год.

***
## Запуск проекта:

Cоздать и активировать виртуальное окружение:

```
python3 -m venv env
```
```
. env/bin/activate
```
Обновить менеджер пакетов **PIP**:
```
python3 -m pip install --upgrade pip
```

Установить зависимости из файла requirements.txt:

```
pip install -r requirements.txt
```
Создайть в корневой директории файл **.env** из **.env.example** со следующим содержимым:

```
APP_TITLE= Сервис просмотра зарплаты и даты следующего повышения
DESCRIPTION=Description
DATABASE_URL=sqlite+aiosqlite:///./fastapi.db
SECRET=Secret
FIRST_SUPERUSER_EMAIL=login@email.com
FIRST_SUPERUSER_PASSWORD=password
```

Инициализировать **Alembic** в проекте:
```
alembic init --template async alembic 
```

Создать файл миграции:
```
alembic revision --autogenerate -m "migration name"
```

Применить миграции:
```
alembic upgrade head 
```

Загрузить тестовые данные в базу данных:
```
python3 -m app.management.fill_db 
```

Запустить проект:
```
uvicorn app.main:app --reload  
```

Дополнительно добавлена команда очистки базы данных:
```
python3 -m app.management.clear_db
```

***
##  Примеры работы с API проекта:
##### Сервис доступен по адресу:

- http://127.0.0.1:8000 - главная страница сервиса;

**Система авторизации в Swagger работоспособна**

##### Документация к API досупна по адресам:

- http://127.0.0.1:8000/docs - Swagger.
- http://127.0.0.1:8000/redoc - Redoc.

    **Просмотр занимаемой должности и даты следующего повышения.**
    Эндпоинт - http://127.0.0.1:8000/promotion/

    * Схема ответа на GET-запрос:
        ```json
        {
        "position_name": "Тестовый работник",
        "planing_date_of_promotion": "21.07.2025"
        }
        ```
    **Получение заработной платы.**
    Эндпоинт - http://127.0.0.1:8000/salary/

    * Схема POST-запроса:
        ```json
        {
        "date_received": "май 2023"
        }
        ```
    * Схема ответа на POST-запрос:
        ```json
        {
        "official_salary": 15000.43,
        "date_received": "май 2023",
        "income_tax": 0.87,
        "bonus": 25000,
        "total_salary": 34800.37
        }
        ```
***
##  To do:
1. Подключение PostgreSQL.
2. Расширение возможностей проекта (создание CRUD и эндпоинтов для возможности редактирования дат повышений сотрудников различными категориями должностных лиц, совершенствование системы рассчета заработной платы, расширение базы данных).
3. Реализовать возможность собирать и запускать контейнер с сервисом в Docker.
4. Написание тестов на основе Pytest.
